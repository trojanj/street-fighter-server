const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            const err = new Error('User not found');
            err.status = 404;
            throw err;
        }
        return user;
    }

    checkPassword(inputPassword, userPassword) {
        if (inputPassword !== userPassword) {
            const err = new Error('Password is invalid');
            err.status = 401;
            throw err
        }
    }
}

module.exports = new AuthService();