const { UserRepository } = require('../repositories/userRepository');

class UserService {
	create(user) {
		const newUser = UserRepository.create(user);
		if (!newUser) {
			throw new Error('Internal server error!');
		}
		return newUser;
	}

	getUsers() {
		const users = UserRepository.getAll();
		if (!users) {
			const err = new Error('No users in database!');
			err.status = 404;
			throw err;
		}
		return users;
	}

	update(id, dataToUpdate) {
		return UserRepository.update(id, dataToUpdate);
	}

	delete(id) {
		const user = UserRepository.delete(id);
		if (!user.length) {
			const err = new Error('No user with such id!');
			err.status = 404;
			throw err;
		}
		return user;
	}

	search(search) {
		const item = UserRepository.getOne(search);
		if (!item) {
			const err = new Error('No user with such id!');
			err.status = 404;
			throw err;
		}
		return item;
	}
}

module.exports = new UserService();