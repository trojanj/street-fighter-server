const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
	create(fighter) {
		const newFighter = FighterRepository.create(fighter);
		if (!newFighter) {
			throw new Error('Internal server error!');
		}
		return newFighter
	}

	getFighters() {
		const fighters = FighterRepository.getAll()
		if (!fighters.length) {
			const err = new Error('No fighters in database!');
			err.status = 404;
			throw err;
		}
		return fighters
	}

	update(id, dataToUpdate) {
		return FighterRepository.update(id, dataToUpdate);
	}

	delete(id) {
		const fighter = FighterRepository.delete(id);
		if (!fighter.length) {
			const err = new Error('No fighter with such id!');
			err.status = 404;
			throw err;
		}
		return fighter
	}

	search(search) {
		const item = FighterRepository.getOne(search);
		if (!item) {
			const err = new Error('No fighter with such id!');
			err.status = 404;
			throw err
		}
		return item;
	}
}

module.exports = new FighterService();