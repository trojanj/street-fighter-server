const { user } = require('../models/user');

const modelUser = Object.assign({}, user);
delete modelUser.id;

const createUserValid = (req, res, next) => {
	const errors = validateUserInput(req.body);

	if (Object.keys(errors).length) {
		const error = {};
		error.message = Object.values(errors).join('\n');
		error.status = 400;
		res.err = error;
	}

	next();
}

const updateUserValid = (req, res, next) => {
	const errors = validateUserInput(req.body, true);

	if (Object.keys(errors).length) {
		const error = {};
		error.message = Object.values(errors).join('\n');
		error.status = 400;
		res.err = error;
	}

	next();
}

const validateUserInput = (newUser, update) => {
	const errors = {};

	if (!update) {
		Object.keys(modelUser).forEach(key => {
			if (!newUser.hasOwnProperty(key)) errors[key] = `${key} is required!`;
		})
	}

	Object.keys(newUser).forEach(key => {
		if (!modelUser.hasOwnProperty(key)) errors[key] = `${key} is not necessary!`;
	})

	if (newUser.firstName && newUser.firstName.length < 2) {
		errors.firstName = 'First name has to contain at least 2 characters!';
	}

	if (newUser.lastName && newUser.lastName.length < 2) {
		errors.lastName = 'Last name has to contain at least 2 characters!';
	}

	if (newUser.email && !/(\W|^)[\w.+\-]*@gmail\.com(\W|$)/gi.test(newUser.email)) {
		errors.email = 'Email is invalid!';
	}

	if (newUser.phoneNumber && !/^\+380(\d{9})$/g.test(newUser.phoneNumber)) {
		errors.phoneNumber = 'Phone Number has to be +380xxxxxxxxx format!';
	}

	if (newUser.password && newUser.password.length < 3) {
		errors.password = 'Password has to contain at least 3 characters!';
	}

	return errors;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;