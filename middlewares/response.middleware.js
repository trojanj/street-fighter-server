const responseMiddleware = (req, res) => {
	if (res.data) {
		res.json(res.data)
	} else if (res.err && res.err.status) {
		res.status(res.err.status).json({
			error: true,
			message: res.err.message
		})
	} else if (res.err) {
		res.status(500).json({
			error: true,
			message: res.err.message
		})
	} else {
		res.status(500).json({
			error: true,
			message: 'Internal server error!'
		})
	}
}

exports.responseMiddleware = responseMiddleware;