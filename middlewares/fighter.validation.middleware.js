const { fighter } = require('../models/fighter');

const modelFighter = Object.assign({}, fighter);
delete modelFighter.id;

const createFighterValid = (req, res, next) => {
	const errors = validateFighterInput(req.body);	

	if (Object.keys(errors).length > 0) {
		const error = {};
		error.message = Object.values(errors).join('\n');
		error.status = 400;
		res.err = error;
	}

	next();
}

const updateFighterValid = (req, res, next) => {
	const errors = validateFighterInput(req.body, true);	

	if (Object.keys(errors).length) {
		const error = {};
		error.message = Object.values(errors).join('\n');
		error.status = 400;
		res.err = error;
	}
	
	next();
}

const validateFighterInput = (newFighter, update) => {
	const errors = {};
	
	if (!update) {
		Object.keys(modelFighter).forEach(key => {
			if (!newFighter.hasOwnProperty(key)) errors[key] = `${key} is required!`;
		})
	}

	Object.keys(newFighter).forEach(key => {
		if (!modelFighter.hasOwnProperty(key)) errors[key] = `${key} is not necessary!`;
	})

	if (newFighter.name && newFighter.name.length < 2) errors.name = 'Name has to contain at least 2 characters!';

	if (newFighter.power && typeof newFighter.power !== 'number' || newFighter.power > 100 || newFighter.power < 1) {
		errors.power = 'Power has to be a number in range from 1 to 100!';
	} 

	if (newFighter.defense && typeof newFighter.defense !== 'number' || newFighter.defense > 10 || newFighter.defense < 1) {
		errors.defense = 'Defense has to be a number in range from 1 to 10!';
	} 

	if (newFighter.health && typeof newFighter.health !== 'number' || newFighter.health < 1) {
		errors.health = 'Health has to be a number greater than 1!';
	} 

	return errors
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;