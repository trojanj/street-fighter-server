const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    const users = UserService.getUsers()
    res.data = users;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.get('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const user = UserService.search({id});
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', createUserValid, function(req, res, next) { 
  try {
    if (res.err) return next();
    
    const user = UserService.create(req.body);
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', updateUserValid, function(req, res, next) {
  try {
    if (res.err) return next();
    
    const id = req.params.id;
    const dataToUpdate = req.body;
    UserService.search({id});
    const user = UserService.update(id, dataToUpdate);
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const user = UserService.delete(id);
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)


module.exports = router;