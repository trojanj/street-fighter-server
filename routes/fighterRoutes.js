const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    const fighters = FighterService.getFighters();
    res.data = fighters;
  } catch(err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

router.get('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const fighter = FighterService.search({id});  
    res.data = fighter;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', createFighterValid, function(req, res, next) {
  try {
    if (res.err) return next();

    const fighter = FighterService.create(req.body);
    res.data = fighter;
  } catch(err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', updateFighterValid, function(req, res, next) {
  try {
    if (res.err) return next();
    
    const id = req.params.id;
    const dataToUpdate = req.body;
    FighterService.search({id});
    const fighter = FighterService.update(id, dataToUpdate);
    res.data = fighter;
  } catch (err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

router.delete('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const fighter = FighterService.delete(id);
    res.data = fighter;
   } catch (err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

module.exports = router;